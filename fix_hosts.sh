#!/usr/bin/env bash
##
## Break down of iptroll failures by modules
##

# Date for CSV file to be read from
#rundate=$(date +%Y%m%d 2>&1)
rundate=$(date +%Y%m24 2>&1)
cur_day=$(date +%d 2>&1)
cur_month=$(date +%m 2>&1)
if [ $cur_day -lt 24 ];
then
	cur_month=$((10#$cur_month));
	let 'new_month = cur_month - 1';
	let 'cur_month = new_month';
	rundate=$(date +%Y 2>&1)0"${cur_month}"24
fi

#
# This is the file that holds the data for all modules being 
# checked.
csvList=/data/sysadm/iptroll/data/netsummary_$rundate

#
# This in the folder location where all the generated data 
# should be dumped
#
# Failed hosts list
f_list=/users/tqfl/scripts/iptroll/failed_boxes_$rundate
# Failed modules
fm_list=/users/tqfl/scripts/iptroll/mods

# Location to search for iptroll modules
mods=$(ls /util/iptroll/modules.d/ 2>&1)

# This line gets the line listing the hosts iptroll ran across 
hosts=$(head -1 $csvList 2>&1)

# Break the line into an array of strings
# Where each array element represents a host
OIFS="$IFS" # Internal Field Seperator
IFS=$' '
read -a host <<< "${hosts}" # Array of hosts
mod=($(ls /util/iptroll/modules.d)) # Array of Modules
IFS=$'\r\n'
line=($(cat $csvList))
IFS=$OIFS

# Main function for all activities
main()
{
	if [ -f $f_list ];
	then
		rm -f $f_list;
	fi
	for ((i=1;i < ${#host[@]};i++)); 	# Go through all the hosts in the 
	do					# in the csv file
		echo "##"	
		echo "## Client ${host[$i]}";
		echo "##               "
		for ((j=2; j < ${#line[@]}; j++)); 	# Go through each module line and sieve out the hosts that failed
		do					# on each module	
			
			module=$(echo ${line[j]}|cut -d' ' -f 1 2>&1);
			report=$(echo ${line[j]}|awk -v temp="$i" '{print $temp}' 2>&1);
			if [ "$report" = "FAILED" ];
			then
				echo "${host[$i]}:$module:$report" |tee -a $f_list 2>&1
			fi
		done;
		echo -e "\n";
	done;
}

main
