#!/bin/bash
#################################################################
## The scripts function is to edit users home environment with ##
## the following lines					       ##
##							       ##
## setenv ECLRUNPATH /vend/slb/eclrun_utils/2013.1.1           ##
## set path = ($ECLRUNPATH/macros $path)		       ##
## source $ECLRUNPATH/macros/@eclrunsetup.csh		       ##
##							       ##
## Author: Tony Jonah					       ##
##							       ##
#################################################################
## Variables
user_list=list

for user in `cat $user_list`;
do
	echo "Working on the env file for $user..............";
	sleep 2;
	echo "################### INTERSECT ENV ###################" >> /users/$user/user.env;
	echo "setenv ECLRUNPATH /vend/slb/eclrun_utils/2013.1.1" >> /users/$user/user.env;
	echo "set path = ('$ECLRUNPATH'/macros '$path')" >> /users/$user/user.env;
	echo "source '$ECLRUNPATH'/macros/@eclrunsetup.csh" >> /users/$user/user.env;
done;
