#!/usr/bin/env python
import subprocess
import os
import paramiko
from multiprocessing import Process


##
## Fix Iptroll
##

def runInParallel(*fns):
  proc = []
  for fn in fns:
    p = Process(target=fn)
    p.start()
    proc.append(p)
  for p in proc:
    p.join()


def runIptroll(hostlist):
    port = 22
    username = root
    passw = LHFcb@o1
    for host in hostlist:
        s = paramiko.SSHClient()
        s.load_system_host_keys()
        s.connect(host,port,username,passw)
        cmd = 'ssh -q '+host+' /util/iptroll/bin/iptroll >> /tmp/tonys_'+host;'echo -e "\n\n" >> /tmp/tonys_'+host
        subprocess.call(cmd,shell=True)
        cmd1 = 'echo -e "\n\n" >> /tmp/tonys_'+host
        stdin, stdout, stderr = s.exec_command(cmd)
        subprocess.call(cmd1,shell=True)


if __name__ == "__main__":
    # List of machines to work on
    host_list = "/tmp/tonys_host_rerun"
    # Break list into two smaller lists
    with open(host_list) as f:
        line = f.readlines()
    line = [x.strip() for x in line]

    hosts_list_a = line[:len(line)/2]
    hosts_list_b = line[len(line)/2:]
    print("List a is ")
    print(hosts_list_a)
    print("List b is")
    print(hosts_list_b)
    runInParallel(runIptroll(hosts_list_a),runIptroll(hosts_list_b))


