#!/bin/bash
##
## Script works on files listed to reassign
## files to new users or groups
##
echo -n "Please enter the name of the file with the listing: "
read list
echo -n "Please enter the name of the owner of these files: "
read leaver
echo -n "Enter the name of the person the files should be reassigned to: "
read reassignee
id -ur $reassignee
RESULT=$?
if [ $RESULT -eq 0 ]; then
	for item in `cat $list`;do
		chown $reassignee $item	
	done;
else
	echo "$reassignee is Not a valid userid";
	return 0;
fi
echo "All done!!"
echo "Check listed_files.$leaver to see listing of files"
