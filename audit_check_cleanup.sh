#!/usr/bin/env bash
for host in lklnx3{78..81};
do	
	ssh -q $host 'cat /tmp/au_rpt |grep -v "===\|date time event\|TTY Report\|events" | sed "s/<up>//g"| sed "s/<down>//g"| sed "s/<left>//g" | sed "s/<right>//g" | sed "s/<backspace>//g" | sed "s/<ret>//g" | sed "s/,,//g"|sed "s/<nl>//g"| sed "s/<tab>//g" |sed "s/<esc>//g"| sed "s/<delete>//g" |sed "s/,/ /g" > /var/log/auditreport';
done;
