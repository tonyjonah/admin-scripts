#!/usr/bin/env python

import os
import sys
import subprocess


## Parse text generated

def checkFailed(log):
	counter = 0
	marker = 'off'
## Go through report line by line
	for line in log:
## Find the line that has unsuccessfull saveset
		components = line.split(" ")
		for str in components :
			#print str
			if (str == "Unsuccessful") :
				print line[-31:]
				marker = 'on'
				counter = 0
				break
			else :
				continue
				
		if ( len(components) > 5 and marker == "on" ) :
			print line
			continue
		
		elif ( len(components) == 5 and marker == 'on' ) :
			if line[-1:] == ":" :
				print line
				counter = counter + 1
			if counter == 2 : marker = 'off'
			continue			
			
		else :
			if marker == 'off' :
				continue
						

			
if __name__ == "__main__":

## Copy away messages file and make the file accessible
    cmd1 = "/usr/bin/scp /var/log/messages /tmp/backup_messages"
    subprocess.call(cmd1,shell=True)

    infile_name = "/tmp/backup_messages"
    try:
        infile = open(infile_name, 'r')
    except IOError:
        print "We could not access the /tmp/backup_messages"
        print __doc__
        sys.exit(1)
## Parse through log
    checkFailed(infile)

## note line and go through for all lines until there is a line with two empty lines
