#!/bin/bash
##
## Script to turn off the rsh service on hosts
##
hosts=/RDIST/hosts_all;
log=linux_rsh_log_max
#for host in `cat $hosts`;
for host in lklnx224 lklnx227
do
	osname=$(ssh -q $host uname -s 2>&1);
	echo "$host is a $osname host" 2>&1 >> $log;
	if [ "$osname" == "Linux" ];
	then
		echo "Attempting to Kill rsh on $host" 2>&1 >> $log;
		/sysadm/sbin/ssh4s $host chkconfig rlogin off 2>&1 >> $log;
		/sysadm/sbin/ssh4s $host chkconfig rsh off 2>&1 >> $log;
		if [ $? -eq 0 ];
		then
			echo "rsh has been disabled on $host" 2>&1 >> $log;
		else
			echo "rsh was not turned off on $host" 2>&1 >> $log;
		fi;
	fi;
	echo "" 2>&1 >> $log;
done;
