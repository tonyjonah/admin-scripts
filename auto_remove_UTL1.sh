#!/usr/bin/env bash

# This script removes the full tapes from UTL1 which are to be sent
# to the offsite storage location. It runs automatically when run

nsrjb -II -j lklnx376_UTL1 > /dev/null
cd /legadmin/tapes/UTL1
rm -f utl1_slots;
count=0;
for tape in `cat /tmp/list_for_UTL1`;
do 
	echo -n " $tape" >> utl1_slots;
	((count=count+1));
	if [ $count -eq 10 ];
	then 
		nsrjb -w -j lklnx376_UTL1 `cat utl1_slots`;
	else 
		continue;
	fi;
	count=0;
	rm -f utl1_slots;
	sleep 120;
done;
nsrjb -w -j lklnx376_UTL1 `cat utl1_slots`;
