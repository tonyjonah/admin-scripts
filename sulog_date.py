#!/usr/bin/env python
#
#
import subprocess
import os

def main():
	cmd = 'cat /tmp/su.log_date'
	logfile = os.popen(cmd)
	logfile = logfile.read()
	lines = logfile.splitlines()
	for line in lines :
			date = line.split()
			ndate = date[0].split('/')
			yr = ndate[0][-2:]
			mn = ndate[1]
			day = ndate[2]
			newDate = mn + '/' + day + '/' + '20' + yr
			str = line.replace(date[0], newDate)
			subprocess.call("echo " + str + " >> /tmp/su.log_corrected.txt ", shell=True)
			#print str
if __name__ == "__main__":
    main()
