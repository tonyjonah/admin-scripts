#!/usr/bin/env bash
##
## Author: Tony Jonah
## Date	: 
## Scripts dispatches ihosts 
##
rundate=$(date +%Y%m24 2>&1)
cur_day=$(date +%d 2>&1)
cur_month=$(date +%m 2>&1)
if [ $cur_day -lt 24 ];
then
	cur_month=$((10#$cur_month));
	new_month=$((cur_month - 1));
	let 'cur_month = new_month';
	rundate=$(date +%Y 2>&1)0"$cur_month"24
fi
OIFS="$IFS" # Internal Field Seperator
IFS=' ';
mod=($(ls /util/iptroll/modules.d)) # Array of Modules
IFS=$OIFS;
workpath=/data/sysadm/iptroll/logs/

rm -f $workpath/module.*.${rundate}
for line in `cat $workpath/failed_boxes_$rundate`;
do
	sym="$(echo ${line}|cut -d":" -f 2 2>&1)";
	if [[ "${mod[*]}" == *"$sym"* ]];
	then
		host=$(echo ${line}|cut -d":" -f 1 2>&1);
		echo "$host" >> $workpath/module.${sym}.${rundate};
	fi
done;
