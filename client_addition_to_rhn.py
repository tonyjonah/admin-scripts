#!/usr/bin/env bash
##
## Script to join machines to RHN
##
rm -f /etc/sysconfig/rhn/systemid
sed -i 's/enabled = 1/enabled = 0/' /etc/yum/pluginconf.d/rhnplugin.conf
/bin/rpm -Uvh http://lklnx553.lagnr.chevrontexaco.net/pub/katello-ca-consumer-latest.noarch.rpm;
/usr/sbin/subscription-manager register --org CVX --activationkey RHEL6_Server
/sbin/chkconfig rhsmcertd on;/sbin/service rhsmcertd start  
