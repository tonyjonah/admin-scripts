#!/bin/bash
##
## Script to force systems to bind to lklnx210 and lklnx212
## Date: 28-January-2014
##

list=/RDIST/hosts_all
systems_off=/users/tqfl/scripts/systems_added_on_lklnx211.all
for host in `cat $list`;
do
	if [ "$(ssh -q $host uname -s 2>&1)" == "Linux" ];
	then
		echo "Now working on $host";
		echo $host >> $systems_off;
		ping -c 2 $host > /dev/null 2>&1
		if [ $? -eq 0 ];then
			/sysadm/sbin/ssh4s $host sed -i '/146.42.134.12/d' /etc/yp.conf;
			echo;
			/sysadm/sbin/ssh4s $host sed -i '/146.42.134.11/d' /etc/yp.conf;
			echo ;
			ssh -q $host sed -i '$a domain lagnr server 146.42.134.11' /etc/yp.conf;
			echo ;
			ssh -q $host sed -i '$a domain lagnr server 146.42.134.12' /etc/yp.conf;
#			/sysadm/sbin/ssh4s $host service ypbind restart;
			echo "Now a look at the /etc/yp.conf file" >> $systems_off;
			/sysadm/sbin/ssh4s $host tail /etc/yp.conf >> $systems_off;
			echo "" >> $systems_off;
		else
			echo "$host is offline";
			echo "$host is offline" >> $systems_off;
			echo "" >> $systems_off
		fi;
	else
		echo "$host is a SUN/OS box";
		echo "";
	fi;
done;
echo Done!!
