#!/bin/bash

####################################################
##						  ##
##  Date: 6th of March, 2014			  ##
##  Name: Tony Jonah				  ##
##  Function: Find servers with restricted access ##
##						  ##
####################################################

list=/RDIST/linux.servers
mylist=/users/tqfl/scripts/restricted_servers
for host in `cat $list`
do
	echo $host
	echo $host >> $mylist
	/sysadm/sbin/rsh2s $host cat /etc/passwd|grep + >> $mylist
done;
