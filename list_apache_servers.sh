#!/bin/bash

#
#
#

echo "List of Apache Servers...."
rm -f apachelist.txt
touch  apachelist.txt
for client in `cat /RDIST/hosts_linux_servers`
 do
  if [ $(/sysadm/sbin/ssh4s $client ps -ef | grep httpd |grep -v "grep"| wc -l | awk '{print $1}') -gt 0 ]
   then
    echo "$client" >> apachelist.txt
  fi
 done
echo "************"
mail -s "List of Apache Servers" lagadmin < apachelist.txt
