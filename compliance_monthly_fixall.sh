#!/bin/bash
##
## Script to handle iptroll failures without the admins intervention
##
## Author: Tony Jonah
## Date  : 4th of April, 2015
##

## mnth is the date referencing an Iptroll run
mnth=$(date "+20%y%m24" 2>&1)

## file is the netsummary file used for listing failed iptroll modules on hosts
file=/data/sysadm/iptroll/data/netsummary_$mnth

## lists all the hosts in lekki
list=/RDIST/hosts_all

## file to output information for linux hosts
lin_tmp=/tmp/lnxhosts

## file to output information for Solaris hosts
sol_tmp=/tmp/solhosts

hostline=$(sed '1q;d' $file 2>&1)
IFS=' ' read -a host <<< "${hostline}"
for i in "${host[@]}"
do
	if [ $i -eq 0 ];then
		continue;
	fi
	let count=0;
	for line in `cat $file`;
	do
		if [ $count -eq 0 ]; then
			let "count=count+1";
			continue;
		elif [ $count -eq 1 ]; then
			let "count=count+1";
			continue;
		else
			let "count=count+1";
			ping -c 1 ${host[$i]} 2>&1;
        		if [ $? -eq 0 ];then
                		osname=$(ssh -q ${host[$i]} "uname -s" 2>&1)
                		if [ "$osname" -eq "Linux" ];then
                        		echo ${host[$i]} | tee $lin_tmp;
					IFS=' ' read -a token <<< ${line}
					for j in "${token[@]}"
					do
						if [ $i -eq $j ];then
							ll
							echo "${host[i]}";
						fi;
					done;
				fi;
			fi;
		fi;
	done;
done;
