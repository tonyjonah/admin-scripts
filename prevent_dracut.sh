#!/usr/bin/env bash
##
##  This script was created to include the boot partition inside the grub definitions of a server
##  We created the script due to the dracut bug which removes the bootable partition from the grub line
##

cmmd='df -hl|grep -v " \/."|grep -v "Filesystem"|cut -d" " -f1';
list=/RDIST/linux.servers
for host in $(cat $list);
do
	ping -c 1 $host > /dev/null;
	if [ $? -eq 0 ];
	then
		echo $host;
#		part=$(/sysadm/sbin/ssh2s $host $cmmd);
#		prtgrb=$(/sysadm/sbin/ssh2s $host "cat /etc/grub.conf|grep 'root='|cut -d'=' -f2|cut -d' ' -f1|grep dev|tail |head -1");
		release=$(/sysadm/sbin/ssh2s $host cat /etc/redhat-release|grep 6);
		if [ -z "$release" ];
		then
#			if [ "$part" == "$prtgrb" ];
#			then
#				echo "They are the same";
#				echo "$prtgrb";
#			else
#				echo "They have changed";
#			fi
			/sysadm/sbin/ssh2s "/usr/bin/scp -q /boot/grub/grub.conf-bkup /boot/grub/grub.conf";
		else
			echo "$host is not a Rhel 6 machine hence not applicable";
		fi
	fi
done
