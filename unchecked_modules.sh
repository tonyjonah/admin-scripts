#!/usr/bin/env bash
##
##

for host in `cat /RDIST/hosts_all`;do
	for mod in exports_world exports_dev exports_systemdirs exports_fqdn exports_rw_root users_procs os_hosts_equiv service_systat;do
		echo "Check host $host for $mod";
		res=$(/sysadm/sbin/ssh8s $host /util/iptroll/modules.d/$mod -c|tail -1 2>&1);
		if [ "$res" = "FAILED" ];
		then
			echo "Fixing $host for module $mod";
			/sysadm/sbin/ssh16s $host /util/iptroll/modules.d/$mod -f;
		else
			continue;
		fi
	done;
	/sysadm/sbin/ssh8s $host "ls -al /etc/hosts.equiv";
	echo "";
done;
