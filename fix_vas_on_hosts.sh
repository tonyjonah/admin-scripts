#!/usr/bin/env bash
##
##

entry="post-auth-script = /usr/sbin/vas-auth-logger"
## Navigate to the VAS install location
cd /data/sysadm/VAS/vas.3.5.2-56.sol/install.d

for host in $(cat /tmp/serverlist);
do
	echo "$host" >> /tmp/report_on_fix;
	ssh -q $host /bin/rpm -Uvh /data/sysadm/VAS/vas.3.5.2-56.sol/software.d/linux-x86_64.d/vasclnt*.rpm >> /tmp/report_on_fix ;
	ssh -q $host /bin/rpm -Uvh /data/sysadm/VAS/vas.3.5.2-56.sol/software.d/linux-x86_64.d/vasgp*.rpm >> /tmp/report_on_fix;
	ssh -q $host /bin/rpm -Uvh /data/sysadm/VAS/vas.3.5.2-56.sol/software.d/linux-x86_64.d/quest-openssh-5.2p1_q1-1.x86_64.rpm >> /tmp/report_on_fix;
	ssh -q $host awk '/pam_vas/ { print; print " post-auth-script = /usr/sbin/vas-auth-logger"; next }1' /etc/opt/quest/vas/vas.conf >> /tmp/report_on_fix;
done;