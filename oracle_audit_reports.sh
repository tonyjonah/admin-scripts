#!/bin/env bash
##
##
##
rdate=`date +'%d-%m-%y'`
echo -e "This is the daily report for activities which occured \n during the day.\n" > /tmp/oracle_audit 2>&1;
echo "" >> /tmp/oracle_audit 2>&1;
/sbin/aureport --start today --tty >> /tmp/oracle_audit 2>&1;

## Filter report
## Remove up
sed -i 's/<up//g' /tmp/oracle_audit

## Remove down
sed -i 's/<down//g' /tmp/oracle_audit

## Remove left
sed -i 's/<left//g' /tmp/oracle_audit

## Remove right
sed -i 's/<right//g' /tmp/oracle_audit

## Remove ret
sed -i 's/<ret//g' /tmp/oracle_audit

## Remove tab
sed -i 's/<tab//g' /tmp/oracle_audit

## Remove nl
sed -i 's/<nl//g' /tmp/oracle_audit

## Remove esc
sed -i 's/<esc//g' /tmp/oracle_audit

## Remove >
sed -i 's/>//g' /tmp/oracle_audit

for line in `cat /tmp/oracle_audit`;
do
	user_id=$(echo ${line}|awk "{ print $5 }");
	user=$(getent passwd ${user_id}|cut -d":" -f1);
	sed -i 's/${user_id}/${user}/g' /tmp/oracle_audit;
done;

#mail -s "Daily Audit Report $rdate" rqvn apty omno ogte < /tmp/oracle_audit
mail -s "Daily Audit Report $rdate" tqfl < /tmp/oracle_audit
