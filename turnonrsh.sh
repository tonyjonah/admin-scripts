#!/bin/bash
##
## Script to turn on the rsh service on hosts
##
hosts=/RDIST/linux_oracle;
log=/tmp/rsh_status
for host in `cat $hosts`;
do
	ping -c ${host} > /dev/null 2>&1;	
	if [ $? -eq 0 ];
	then;
		osname=$(ssh -q $host uname -s 2>&1);
		echo "$host is a $osname host" >> $log 2>&1;
		if [ "$osname" == "Linux" ];
		then
			echo "Confirming that rsh has been disabled on $host" >> $log 2>&1;
			status=$(/sysadm/sbin/ssh8s $host /sbin/chkconfig --list rsh|grep off )
			if [ -n ${status} ];
			then
				echo "rsh has been turned off for $host" >> $log 2>&1;
			else
				if [ -n $(/sysadm/sbin/ssh8s $host /sbin/chkconfig --list rsh|grep on) ];
				then				
					echo "rsh is ON for $host" >> $log 2>&1;
				fi
			fi;
		else	
			echo "Confirming that rsh has been disabled on $host" >> $log 2>&1;
			if [ -n $(/sysadm/sbin/ssh8s $host inetadm | grep -i rlogin|grep disabled) ];
			then
				echo "rsh has been turned off for $host" >> $log 2>&1;
			else
				if [ -n $(/sysadm/sbin/ssh8s $host inetadm | grep -i rlogin|grep enabled) ];
				then				
					echo "rsh is ON for $host" >> $log 2>&1;
				fi
			fi;
		fi;
		echo "" >> $log 2>&1;
	else
		echo "$host is offline" >> $log 2>&1;
	fi
done;
