#!/usr/bin/env bash
## Script functions to clear mirror disk /etc/fstab file
## of entries of the live. This is to ensure the server boots 
## to the mirror disk when an Admin tries to boot it to the mirror.

/bin/mv /mirror_root/etc/fstab /mirror_root/etc/fstab.old
/bin/cat /mirror_root/etc/fstab.old | /bin/grep -v mirror | /bin/sed 's/c0d0/c0d1/' > /mirror_root/etc/fstab
diff /mirror_root/etc/fstab.old /mirror_root/etc/fstab
