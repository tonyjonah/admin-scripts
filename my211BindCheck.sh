#!/bin/bash
##
## Check for systems bound to lklnx211
##

list=/RDIST/hosts_all

echo "Please enter the name of the NIS server to check : ";
read server;
for host in `cat $list`;do
	check=$(/sysadm/sbin/ssh2s $host ypwhich 2>1&);
	if [ "$check" == "lklnx211.lagnr.chevrontexaco.net" ];
	then
		echo Now on $host;
		echo $host >> ./systems_on_lklnx211;
	fi;
done;
