#!/usr/bin/env bash


touch /tmp/hosts_resolution_log

src_file=/domain/src/hosts
log=/tmp/hosts_resolution_log
RESTORE='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
OIFS=$IFS
IFS=$'\n'

for entry in $( cat $src_file | grep "lklnx\|eclnx\|gblnx" );do
    ## Acquire Ip address for host being traversed
    ## in loop
    ip_addr=$(echo ${entry} | awk '{ print $1 }')

    ## Acquire host name for host as well
    host=$(echo ${entry} | awk '{ print $2 }' | cut -d"." -f1)

    ## Confirm if host is commented out
    decommissioned=$(echo ${entry} | grep -E "^#")
    if [ $? -eq 0 ];then
        printf "\n%s" "${ip_addr} mapping to ${host}" ; echo -e " ${RED}=> COMMENTED${RESTORE}"
        continue
    fi

    ## Ping machines to confirm if they are online
    ping_res=$(ping -c 3 $ip_addr > /dev/null 2>&1)

    if [ $? -eq 0 ];then
        nslkup_res=$(nslookup ${ip_addr}|grep name|awk '{ print $4 }'|cut -d"." -f1)

        ## Confirm host is pingable and the host name is 
        ## equal to the assigned hostname
        if [ "${host}" = "${nslkup_res}" ]; then
            printf "\n%s" "${host} mapping to ${ip_addr}" ; echo -e " ${GREEN}=> OK${RESTORE}" 
        else
            printf "\n%s" "${host} mapping to ${ip_addr}" ; echo -e " ${RED}=> NOT OK${RESTORE}" 
        fi
    
    else
        printf "\n%s" "${ip_addr} which points to ${host}" ; echo -e " ${RED} => NOT Pingable${RESTORE}" 
    fi
done
IFS=$OIFS