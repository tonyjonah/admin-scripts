#!/bin/bash

nohup yum install net-snmp net-snmp-libs net-snmp-utils
sleep 3
service snmpd start
sleep 3
chkconfig snmpd on
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.old
cp /data/sysadm/linux/Linux_Servers/snmpd.conf /etc/snmp/
sleep 3
service snmpd reload > /dev/null 2>&1
