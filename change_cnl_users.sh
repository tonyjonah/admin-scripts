#!/bin/bash
##
## This script was written to change the group owner for all listed files
## and replace them with the second argument
## 
## Author	: Tony Jonah
## Date		: 31-Dec-2014
##
for line in `cat /data/sysadm/grpuser_filesowned/files.wowdba`;
do 

	if [ "wowdba" = "$(ls -l $line |awk '{print $3}' 2>&1)" ];
	then
		echo "Found a wowdba owned file";
		chown sysadm $line;
	else
		echo "The file $line is not owned by wowdba";
		echo "skipping";
	fi
done;
