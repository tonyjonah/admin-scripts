#!/bin/ksh
##
## Script to detect open access to client's xserver
##
classb=xxx.xx.
floor=10
#while [[ $floor -lt 256 ]]; do

##
## Generates list with Ip addresses for IT hosts and blades
##
for host in `cat /RDIST/hosts_all`;
do 
	ping -c 2 $host > /dev/null 2>&1;
	if [ $? -eq 0 ];then
		add=$(nslookup $host|tail -2|grep Address|cut -d":" -f 2); echo -n " ${add}" >> /tmp/ithosts 2>&1;
	else
		continue;
	fi
done;

unset $host;

for host in `cat /tmp/ithosts`;
do
#	UP="down"
#      	for addr in `nmap -oG - -p 6000 "${classb}${floor}.0/24" | grep open | awk '{print $2}'`; do
	portState=$(nmap -p 6000 ${host} | grep open | awk '{print $2}');
#      	xwd -root -screen -silent -display ${addr}:0.0 >/dev/null 2>&1 && nslookup ${addr} | grep "name =" >>ipaddresses &
       	openhost=$(xwd -root -screen -silent -display ${host}:0.0 > /dev/null 2>&1 && nslookup ${host} | grep "name =" >> /tmp/ipaddresses &)
       	sleep 9
       	kill `ps -ef | egrep "xwd -root -screen -silent -display" | awk '{print $2}'` 2> /dev/null
#	done
	sleep 1
#	let octet=$octet+1
#	echo ${classb}${octet}.0
	if [ "$portState" = "open" ];
	then
		if [ -n "$openhost" ];
		then
			echo " ${host} with address details $openhost has xserver access control open to all" >> /tmp/xopen_mesg;
		else
			echo " ${host} has port 6000 open " >> /tmp/xopen_mesg;
		fi
	fi 
	
done;

mail -s "List of Systems with X open" tqfl aruf lagadmin < /tmp/xopen_mesg

# Delete generated list of hosts
[ -f /tmp/ithosts ] && rm -f /tmp/ithosts /tmp/xopen_mesg /tmp/ipaddresses;
