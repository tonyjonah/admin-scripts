#!/usr/bin/env python
##
## Tokenize lines of audit report
##
import sys


def tokenize_report(line):
	'''return a dictionary of the pertinent pieces of an oracle audit generated log file
	Currently, the only fields we are interested in are sequence, id, session and the command.
	'''
	split_line = line.split(" ")
	num_line = len(split_line)
	outputLine = ""
	if num_line < 1:
		# code for  empty lines
		return outputLine
	
	elif num_line == 1:
		#code for single arguments
		outputLine = split_line[0]
		return outputLine
		
	elif num_line == 5:
		#code for 5 arguments
		for i in range(0, 5):
			outputLine = outputLine+split_line[i]+" "
		return outputLine
		
	elif ( num_line >= 7 and ( split_line[5] == "?" ) ):
		#code for  arguments greater than 7
		# Which is the line with all the 
		for i  in range(0, num_line - 1):
			if ( i >= 0 and i  <=7  ) :
				outputLine = outputLine+split_line[i]+", "
			else:
				outputLine = outputLine+split_line[i]+" "
		return outputLine

	elif num_line == 9:
		#code for 10 arguments
		for i in range(0, num_line - 1):
			outputLine = outputLine+split_line[i]+" "
		return outputLine		
	
	elif num_line == 10:
		#code for 10 arguments
		for i in range(0, num_line - 1):
			outputLine = outputLine+split_line[i]+" "
		return outputLine 
		
	else:
		return num_line
	

def generate_csv_report(report):

	report_line = {}
	for line in report:
		line_dict = tokenize_report(line)
		print line_dict
	return line_dict


if __name__ == "__main__":
	if not len(sys.argv) > 1:
		print __doc__
		sys.exit(1)
	infile_name = sys.argv[1]
	try:
		infile = open(infile_name, 'r')
	except IOError:
		print "You must specify a valid file to parse"
		print __doc__
		sys.exit(1)
	log_report = generate_csv_report(infile)
	print log_report
	infile.close()
