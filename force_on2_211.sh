#!/bin/bash
##
## Script to force IT systems to bind to lklnx211 alone
## Date: 28-January-2014
##

list=/users/tqfl/scripts/IT_systems
systems_on=/users/tqfl/scripts/systems_forced_on_to_lklnx211.IT
for host in `cat $list`;
do
	echo "Now working on $host";
	echo $host >> $systems_on;
	/sysadm/sbin/ssh4s $host sed -i '/146.42.134.10/s/^/#/' /etc/yp.conf;
	/sysadm/sbin/ssh4s $host sed -i '/146.42.134.12/s/^/#/' /etc/yp.conf;
	/sysadm/sbin/ssh4s $host service ypbind restart;
	echo "Now a look at the /etc/yp.conf file" >> $systems_on;
	/sysadm/sbin/ssh4s $host tail /etc/yp.conf >> $systems_on;
	echo "" >> $systems_on;
done;
echo Done!!
