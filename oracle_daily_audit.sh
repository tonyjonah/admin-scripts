#!/bin/env bash
##
## 
## Carry-out daily audit report on oracle servers
##
##
rdate=`date +'%d-%m-%y'`
echo -e "This is the daily report for activities which occured \n during the day.\n" > /tmp/oracle_audit 2>&1;
echo "" >> /tmp/oracle_audit 2>&1;
aureport --start today --tty >> /tmp/oracle_audit 2>&1;
mail -s "Daily Audit Report $rdate" rqvn apty omno ogte < /tmp/oracle_audit 
