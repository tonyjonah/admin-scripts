#!/bin/bash
#################################################
## push_keys                                    #
##                                              #
## Script for pushing ssh authorized keys       #
##                                              #
##                                              #
##                                              #
#################################################
linuxhosts=/RDIST/hosts_all

for host in `cat $linuxhosts`; 
do
	case $host in
        	lklnx210)
                	continue
                	;;
	        lklnx210.lagnr.chevrontexaco.net)
        	        continue
	                ;;
	        lklnx211)
        	        continue
	                ;;
	        lklnx211.lagnr.chevrontexaco.net)
        	        continue
                	;;
	        lklnx212)
        	        continue
	                ;;
        	lklnx212.lagnr.chevrontexaco.net)
                	continue
	                ;;
		lklnx1*|lklnx2*|lklnx30*|lklnx31*|lklnx32*)
			continue
			;;
		ecus*|eclnx*|lkus*|gbus*|gblnx*)
			continue
			;;
	        *)
        	        if [ "`ssh -q $host uname`" == "Linux" ]; then
				echo "Backing up old authorized keys on $host"
                	        ssh -q $host cp /root/.ssh/authorized_keys /root/.ssh/authorized_keys.14Oct14
				scp -p /users/tqfl/scripts/keys/authorized_keys.new $host:/root/.ssh/authorized_keys
	                else
        	                echo "Backing up old authorized keys on $host"
				ssh -q $host cp /.ssh/authorized_keys /.ssh/authorized_keys.14Oct14
				scp -p /users/tqfl/scripts/keys/authorized_keys.new $host:/.ssh/authorized_keys
			fi
		;;
	esac
done;
echo "Done!!";
