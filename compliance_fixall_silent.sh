#!/usr/bin/env bash
##########################################################################
##                                                                      ##
##                                                                      ##
## Author	: Tony Jonah                                            	##
## Date  	: 30/12/2013                                           		##
## Modified 	: 15/7/2015                                             ##
##                                                                      ##
##                                                                      ##
## The script carries out specified module checks. The list of hosts    ##
## the script checks against should be placed withing the same folder   ##
## The script should be run as root. Its logs are kept in the same      ##
## directory and can be identified with the date and time               ##
##                                                                      ##
##########################################################################
#
# Variables definition area
#
rundate=$(date +%Y%m24 2>&1)
cur_day=$(date +%d 2>&1)
cur_month=$(date +%m 2>&1)
if [ $cur_day -lt 24 ];
then
        new_month=$((10#$cur_month - 1));
        let 'cur_month = new_month';
        rundate=$(date +%Y 2>&1)0"$cur_month"24
fi
## Time when the script runs
TIME="`date '+%d%b%y'`"

RET=0

## Mail 

## Module location for module listed hosts that failed
modLoc=/data/sysadm/iptroll/logs/

## Hosts failed on current module
hostfiles=/tmp/mods

## Module listing
mods=/util/iptroll/modules.d/

## Resolution Log
res_log=/data/sysadm/iptroll/logs/iptroll_result.${rundate}

##
## Section where the functions housing the Iptroll checks and fixes are actually implemented
##

## Function to fix snmp related issues
function fixSnmp()
{
	host=$1
	module=$2
	check=$(ssh -q $host $mods/$module -c |tail -1 2>&1);
	if [ "$check" = "Passed" ]; then
		return 1;
	else
		ssh -q ${host} "/bin/cp /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.$rundate; sed -i 's/^com2sec/#com2sec/1' /etc/snmp/snmpd.conf ;chkconfig smpd on"
		res=$(ssh -q $host ${mods}/$module -c|tail -1  2>&1);
		if [ "$res" = "Passed" ]; then
			return 0;
		else 
			return 1;
		fi
	fi
}

## Function to fix root_net_login
function fixRootNetLogin()
{
	host=$1;
	module=$2;
	check=$(ssh -q ${host} $mods/$module -c |tail -1 2>&1);
	if [ "$check" ="Passed" ]; then
		return 1
	else
		ssh -q ${host} "sed -i '/^rsh*/d' /etc/securetty"
		res=$(ssh -q $host ${mods}/$module -c|tail -1  2>&1);
		if [ "$res" = "Passed" ]; then
			return 0;
		else 
			return 1;
		fi
	fi
}

## Function to fix os_pwd_rl_one
function fixOsPwdRLOne()
{
	host=$1
	module=$2
	check=$(ssh -q $host $mods/$module -c |tail -1 2>&1)
	if [ "$check" = "Passed" ]; then
		return 1
	else
		ssh -q ${host} "sed -i 's:^SINGLE=/sbin/sushell:SINGLE=/sbin/sulogin:' /etc/sysconfig/init"
		res=$(ssh -q ${host} ${mods}/${module} -c|tail -1  2>&1);
		if [ "$res" = "Passed" ]; then
			return 0;
		else 
			return 1;
		fi
	fi
}

## Function to fix users_sshkeys_nis
function fixUsersSSHkeyNis()
{
	host=$1
	module=$2
	check=$(ssh -q $host $mods/$module -c |tail -1 2>&1)
	if [ "$check" = "Passed" ]; then
		return 1
	else
		ssh -q ${host} "for item in $(cat /tmp/users_sshkey_nis.content);do rm -rf ${item};done"
		res=$(ssh -q ${host} ${mods}/${module} -c|tail -1  2>&1);
		if [ "$res" = "Passed" ]; then
			return 0;
		else 
			return 1;
		fi
	fi
}

#fixrootnetlogin
#  ssh -q ${host} "sed -i '/rsh/d' /etc/securetty "

## Function to fix os_banner_sendmail
function fixOsBannerSendmail()
{
	host=$1
	module=$2
	check=$(ssh -q $host $mods/$module -c |tail -1 2>&1)
	if [ "$check" = "Passed" ]; then
		return 1
	else
		# conf=$(ssh -q ${host} "/util/iptroll/modules.d/os_banner_sendmail -c|grep After" 2>&1)
		ssh -q ${host} 'newmsg="SmtpGreetingMessage=";smtpmsg="SmtpGreetingMessage=\$j Sendmail \$v/\$Z; \$b";sed -i "s@$smtpmsg@$newmsg@g" /etc/mail/sendmail.cf; service sendmail restart'
		res=$(ssh -q ${host} ${mods}/${module} -c|tail -1  2>&1)
		if [ "$res" = "Passed" ]; then
			return 0;
		else
			return 1;
		fi
	fi
}

## General function to Fix or link modules needing
## Scripted fixes to the functions that would fix them
function fixer()
{

	printf "\n**Working on host %s for module %s \n" "$1" "$2"  >> $res_log 2>&1
	
	## Run the module in check mode
	res=$(ssh -q $1 ${mods}/$2 -c  2>&1);
	
	## Send the result to the resolution log file
	echo "$res" >> $res_log 2>&1

	## Assign the result of the module run to the variable result
	result=$(tail -1 $res_log 2>&1)
	result1=$(tail -2 $res_log | grep "FAILED" 2>&1)
	
	## Confirm that the result failed
	if [ "$result" = "FAILED" ] || [ "$result1" = "FAILED" ];
	then
	
		## Find out the failed module and try the customized fix
		if [ "$2" = "service_snmp" ]; then
			host=$1
			module=$2
			ret=fixSnmp $host $module
			if [ $ret -eq 0 ]; then
				printf "\n** Successfully Fixed module $2 on host $1" >> $res_log 2>&1
				mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
				break;
			fi
			rm -f /tmp/fixLog;
		elif [ "$2" = "root_net_login" ]; then
			host=$1
			module=$2
			ret=fixRootNetLogin $host $module;
			if [ $ret -eq 0 ]; then
				printf "\n** Successfully Fixed module $2 on host $1" >> $res_log 2>&1
				mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
				break;
			fi
			rm -f /tmp/fixLog;
		elif [ "$2" = "os_pwd_rl_one" ]; then
			host=$1
			module=$2
			ret=fixOsPwdRLOne $host $module;
			if [ $ret -eq 0 ]; then
				printf "\n** Successfully Fixed module $2 on host $1" >> $res_log 2>&1
				mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
				break
			fi
			rm -f /tmp/fixLog;
		elif [ "$2" = "users_sshkey_nis" ]; then
			host=$1
			module=$2
			ret=fixUsersSSHkeyNis $host $module
			if [ $ret -eq 0 ]; then
				printf "\n** Successfully Fixed module $2 on host $1" >> $res_log 2>&1
				mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
				break
			fi			
			rm -f /tmp/fixLog;
		elif [ "$2" = "os_banner_sendmail" ]; then
			host=$1
			module=$2
			ret=fixOsBannerSendmail $host $module
			if [ $ret -eq 0 ]; then
				printf "\n** Successfully Fixed module $2 on host $1" >> $res_log 2>&1
				mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
				break
			fi

		elif [ "$2" = "" ]; then
			host=$1
			module=$2
			#ret=""
		else
		
			rm -f /tmp/fixLog;
		fi
        fix_res=$(/usr/bin/ssh -q $1 ${mods}/$2 -f  2>&1);
		echo "$res" >> $res_log 2>&1
		echo "$fix_res" >> $res_log 2>&1
		echo "$res" > /tmp/fixLog 2>&1
		fin_res=$(tail -1 $res_log 2>&1)

		if [ "$fin_res" = "Deferred" ];
		then
			mail -s "Work on Compliance module $2 for Host $1 for $rundate" lagadmin < /tmp/fixLog;
		fi
	fi
}

##
## Section creates a temporary list of module files for current module being worked on
##
function locate()
{
	rm -f $hostfiles;
	find /data/sysadm/iptroll/logs/ -name "module.*.${rundate}" -exec echo  >> /tmp/mods 2>&1 {} \;
}

function workList()
{
	for list in `cat $hostfiles`;
	do
		module=$(echo $list |cut -d"." -f 2)
		for host in `cat $list`;
		do
			echo "$host is now being fixed on $module";
			fixer $host $module
		done;
	done;
}

##
## Controller function
##
main()
{
	# Locate files
	locate
	
	# Parse List of files
	# Work on each file individually and log the results of each fix
	# Those that fail send an email to the admins managing the server to notfiy them of the anomaly
	workList	

}

main
