#!/usr/bin/env bash
/usr/bin/rsync -a / /mirror_root/ >> /var/log/rsynmirror 2>&1;
if [ $? -eq 0 ];
then 
	echo -e "\n##\t Copy of mirror from root was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
	echo -e "\n##\t The backup of / to mirror_root failed \n\n##" >>  /var/log/rsynmirror 2>&1;
fi

echo -e "\n Commencing the backup of the /boot/ partition to /mirror_boot/ \n" >>  /var/log/rsynmirror 2>&1;
/usr/bin/rsync -a /boot/ /mirror_boot/ >> /var/log/rsynmirror 2>&1 ;
if [ $? -eq 0 ];
then 
	echo -e "\n##\t Copy of mirror_boot from boot was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
	echo -e "\n##\t The backup of /boot/ to mirror_boot failed \n\n##" >>  /var/log/rsynmirror 2>&1;
fi

echo -e "\n Commencing the backup of the /local/ partition to /mirror_local/ \n" >>  /var/log/rsynmirror 2>&1;
/usr/bin/rsync -a /local/ /mirror_local/ >> /var/log/rsynmirror 2>&1 ;
if [ $? -eq 0 ];
then 
        echo -e "\n##\t Copy of mirror_local from local was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
	echo -e "\n##\t The backup of /local/ to mirror_local failed \n\n##" >>  /var/log/rsynmirror 2>&1 ;
fi

echo -e "\n Commencing the backup of the /local/lfs0 partition to /mirror_lfs0/ \n" >>  /var/log/rsynmirror 2>&1;
/usr/bin/rsync -a /local/lfs0/ /mirror_lfs0/ >> /var/log/rsynmirror 2>&1 ;
if [ $? -eq 0 ];
then
	echo -e "\n##\t Copy of mirror_lfs0 from local/lfs0 was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
	echo -e "\n##\t The backup of /local/lfs0/ to mirror_lfs0 failed \n\n##" >>  /var/log/rsynmirror 2>&1 ;
fi

echo -e "\n Commencing the backup of the /var partition to /mirror_var/ \n" >>  /var/log/rsynmirror 2>&1;
/usr/bin/rsync -a /var/ /mirror_var/ >> /var/log/rsynmirror 2>&1 ;
if [ $? -eq 0 ];
then
	echo -e "\n##\t Copy of mirror_var from /var was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
	echo -e "\n##\t The backup of /local/lfs0/ to mirror_lfs0 failed \n\n##" >>  /var/log/rsynmirror 2>&1 ;
fi

echo -e "\n Commencing the backup of the /sysadm partition to /mirror_sysadm/ \n" >>  /var/log/rsynmirror 2>&1;
/usr/bin/rsync -a /sysadm/ /mirror_sysadm/ >> /var/log/rsynmirror 2>&1 ;
if [ $? -eq 0 ];
then
        echo -e "\n##\t Copy of mirror_sysadm from /sysadm was successful \n\n" >>  /var/log/rsynmirror 2>&1;
else
        echo -e "\n##\t The backup of /sysadm/ to mirror_sysadm failed \n\n##" >>  /var/log/rsynmirror 2>&1 ;
fi

mail -s "Report of eclnx01's Mirror" tqfl ogte < /var/log/rsynmirror
