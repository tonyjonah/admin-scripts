#!/usr/bin/env bash
## author 	: Tony Jonah
## Date		: 16-10-5
## Revision	: 17-2-22	
##

# Clean up report
/data/sysadm/scripts/audit_check_cleanup.sh
year=$(date '+%y');
yr=$(date '+%y');
mon=$(($(echo "$(date '+%m')") - 1));
day=$(date '+%d');
su_log=/var/log/su.log;
report_date="$(echo "$(date --date=$(echo "$mon/$day/$yr")|awk '{print $2_$6}')")";
aulog=/var/log/auditreport;
##
##
##
rm -rf /tmp/oracle_audit_report
for server in lklnx378 lklnx379 lklnx380 lklnx381;
do
	echo "Report for oracle server $server" >> /tmp/oracle_audit_report ;
	echo "================================" >> /tmp/oracle_audit_report ;
	ssh -q $server "cat /var/log/auditreport" >> /tmp/oracle_audit_report ;
	sed -i 's/65593/rqvn/g' /tmp/oracle_audit_report;
	sed -i 's/65651/ogte/g' /tmp/oracle_audit_report;
	sed -i 's/21913/apty/g' /tmp/oracle_audit_report;
	sed -i 's/21885/apsy/g' /tmp/oracle_audit_report;
	sed -i 's/21185/oracle/g' /tmp/oracle_audit_report;
	sed -i 's/0 ?/root ?/g' /tmp/oracle_audit_report;
	sed -i 's/,,/ /g' /tmp/oracle_audit_report;
	echo -e "\n\n" >> /tmp/oracle_audit_report ;
	echo "Report on su.log for $server" >> /tmp/oracle_audit_report ;
	echo "_______________________________" >> /tmp/oracle_audit_report ;
	ssh -q $server "cat /var/log/su.log|grep '1$year\/0$mon\|1$year\/$mon'" >> /tmp/oracle_audit_report ;
	echo -e "\n\n" >> /tmp/oracle_audit_report;
done;

# Tokenize Report
/data/sysadm/scripts/oracle_audit_tokenizer.py /tmp/oracle_audit_report > /tmp/oracle_report.csv

# File and Send Report to Manager for Review
echo "Updated Oracle Report Version1"|mutt -s "Oracle Report for $report_date" -a /tmp/oracle_report.csv -- tqfl;
/usr/bin/scp -q /tmp/oracle_report.csv /data/oracle_logs/oracle_report.$report_date.csv > /dev/null 2>&1;
/bin/chown -R toak:toak /data/oracle_logs/oracle_report.$report_date.csv;
/bin/rm -f /tmp/oracle_report.csv;

# Done !!
