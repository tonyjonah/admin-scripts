#!/bin/bash
#####
##### Checks particular modules on a list of machines
#####
echo "This script reads a list of machines and returns the iptroll modules these machines"
echo "";
while [ TRUE ];
do
	echo -n "Please enter the name of the module in iptroll that needs to be checked: ";
	read module;
	dird=/util/iptroll/modules.d/$module;
	echo -n "Also, Please enter the full path of the file listing the hosts to be checked: ";
	read host_list;
	echo "Checking for $module";
	list=$host_list;
	for host in `cat $list`;
	do 
		ping -c 2 $host > /dev/null 2>&1;
		if [ $? -eq 0 ];
		then
			echo -n "$host on " >> /tmp/run_on_$module 2>&1;
			echo "$module" >> /tmp/run_on_$module 2>&1;
			ssh -q $host $dird -c >> /tmp/run_on_$module 2>&1;
			echo >> /tmp/run_on_$module 2>&1;
		fi
	done;
	echo "complete!!!" >> /tmp/run_on_$module 2>&1;
done;
echo "Bye bye";
