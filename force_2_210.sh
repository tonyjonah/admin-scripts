#!/bin/bash
##
## Script to force systems to bind to lklnx210 and lklnx212
## Date: 28-January-2014
##

list=/RDIST/hosts_all
systems_off=/users/tqfl/scripts/systems_taken_off_lklnx211.all
for host in `cat $list`;
do
	echo "Now working on $host";
	echo $host >> $systems_off;
	/sysadm/sbin/ssh4s $host sed -i '/146.42.134.11/d' /etc/yp.conf;
	/sysadm/sbin/ssh4s $host service ypbind restart;
	echo "Now a look at the /etc/yp.conf file" >> $systems_off;
	/sysadm/sbin/ssh4s $host tail /etc/yp.conf >> $systems_off;
	echo "" >> $systems_off;
done;
echo Done!!
