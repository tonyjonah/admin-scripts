#!/usr/bin/env bash
## author 	: Tony Jonah
## Date		: 16-10-5
## Revision	: 17-2-22	
##

# Clean up reports on servers and save in a /var/log/auditreport file
/data/sysadm/scripts/audit_check_cleanup.sh

## Clean up date representation for the report
year=$(date '+%y');
yr=$(date '+%y');
mon=$((10#$(echo "$(date '+%m')") - 1));
day=$(date '+%d');

## Define variable for log location
su_log=/var/log/su.log;

## Print the reports Date representation and append to report
report_date="$(echo "$(date --date=$(echo "$mon/$day/$yr")|awk '{print $2_$6}')")";
aulog=/var/log/auditreport;

## Manager's CAI
mgr=toak

##
##
## Remove old references
rm -rf /tmp/oracle_audit_report;
rm -f su.log_date;

## Go through machine and pick up log
for server in lklnx378 lklnx379 lklnx380 lklnx381;
do
	rm -f /tmp/oracle_audit_report.$server;
	echo "Report for oracle server $server" >> /tmp/oracle_audit_report.$server ;
	echo "================================" >> /tmp/oracle_audit_report.$server ;
	ssh -q $server "cat /var/log/auditreport" >> /tmp/oracle_audit_report.$server ;
	sed -i 's/65593/rqvn/g' /tmp/oracle_audit_report.$server;
	sed -i 's/65651/ogte/g' /tmp/oracle_audit_report.$server;
	sed -i 's/21913/apty/g' /tmp/oracle_audit_report.$server;
	sed -i 's/21885/apsy/g' /tmp/oracle_audit_report.$server;
	sed -i 's/21185/oracle/g' /tmp/oracle_audit_report.$server;
	sed -i 's/0 ?/root ?/g' /tmp/oracle_audit_report.$server;
	sed -i 's/,,/ /g' /tmp/oracle_audit_report.$server;
	echo -e "\n\n" >> /tmp/oracle_audit_report.$server;
	echo "Report on su.log for $server" >> /tmp/oracle_audit_report.$server;
	echo "_______________________________" >> /tmp/oracle_audit_report.$server;
	
	ssh -q $server "cat /var/log/su.log|grep '1$year\/0$mon\|1$year\/$mon'" > /tmp/su.log_date;
	
	/data/sysadm/scripts/sulog_date.py && /bin/cat /tmp/su.log_corrected.txt >> /tmp/oracle_audit_report.$server;
	
	# Remove the recent su.log_corrected file so as not to affect the reports of other servers
	rm -f /tmp/su.log_corrected.txt;
	
	## Clean up the date for the su.log file entries
	echo -e "\n\n" >> /tmp/oracle_audit_report.$server;
done;


# Concatenate all the files and add them into the main report
for server in lklnx3{78..81};
do
	cat /tmp/oracle_audit_report.$server >> /tmp/oracle_audit_report;
done;


# Tokenize Report
/data/sysadm/scripts/oracle_audit_tokenizer.py /tmp/oracle_audit_report > /tmp/oracle_report.csv;

# Send Report as a CSV attachment to the Manager for Review
echo "Updated Oracle Report for $report_date <\\\lkna05\data_oralogs\data\oracle_report.$report_date.csv>"|mutt -s "Oracle Report for $report_date" -- wfod $mgr;

# Copy file to Audit Storage location for Manager
/usr/bin/scp -q /tmp/oracle_report.csv /data/oracle_logs/oracle_report.$report_date.csv > /dev/null 2>&1;

# Change the ownership of the file to the oracle_mgr UNIX group
/bin/chown -R oracle_mgr:oracle_mgr /data/oracle_logs/oracle_report.$report_date.csv;

# Delete the file from lklnx210
/bin/rm -f /tmp/oracle_report.csv;

# Done !!
