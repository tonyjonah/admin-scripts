#!/usr/bin/env bash
##
##

for host in `cat /RDIST/hosts_all`;do
	for mod in service_sadmind service_sprayd service_systatd service_rexecd service_talkd service_fingerd service_rusersd service_rcpd service_rshd service_rlogind service_bootparamd service_rarpd service_rwalld service_rstatd service_chargen service_echo service_dtspcd service_snmp;do
		echo "Check host $host for $mod";
		/sysadm/sbin/ssh4s $host /util/iptroll/modules.d/$mod -c;
	done;
done;
