#!/bin/bash
##
## Script is made to add an entry into the crontab of root
## 
## Author : Tony Jonah
## Date   : 13-Apr-2015
##

list=/RDIST/hosts_linux_clients
file=/tmp/root.cron

entry="45 01 24 * * /util/iptroll/bin/iptroll > /dev/null 2>&1"
entry1="57 15 * * * /util/iptroll/bin/iptroll > /dev/null 2>&1"
for host in `cat $list`;
do
	echo "Now working on $host";
	ping -c 1 $host > /dev/null 2>&1;
	if [ $? -eq 0 ];
	then
		type=$(/sysadm/sbin/ssh4s $host uname -s 2>&1);
		if [ "$type" == "Linux" ];
		then
			echo "$host is linux, now commencing edit"
			/sysadm/sbin/ssh4s $host "crontab -l|tee $file " # Remove after first run has completed
			/sysadm/sbin/ssh4s $host "echo $entry >> $file " # Remove after first run has completed
				#/sysadm/sbin/ssh4s $host "echo $entry1 >> $file "
			if [ $? -eq 0 ];then 
				/sysadm/sbin/ssh4s $host "crontab $file";
				/sysadm/sbin/ssh4s $host "rm -f $file";
				echo "Crontab edit on $host was successful";
				echo "";
			else 
				echo "Crontab edit on $host failed";echo "";
			fi;
		else
			echo "$host is Solaris, edit entry manually";
		fi;
	else
		echo "$host is offline";
		echo "";
	fi;
done;
echo "Edits across all hosts is done";
