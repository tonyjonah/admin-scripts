#!/bin/bash
#####
##### Checks particular modules on a list of machines
#####
echo "This script reads a list of machines and returns the iptroll modules these machines"
echo "";
while [ TRUE ];
do
	echo -n "Please enter the name of the module in iptroll that needs to be checked: ";
	read module;
	dird=/util/iptroll/modules.d/$module;
	echo -n "Also, Please enter the full path of the file listing the hosts to be checked: ";
	read host_list;
	echo "Checking for $module";
	list=$host_list;
	for host in `cat $list`;
	do 
		echo -n "$host on ";
		echo "$module";
		/sysadm/sbin/ssh4s $host $dird -c;
		echo ;
	done;
	echo "complete!!!";
done;
echo "Bye bye";
