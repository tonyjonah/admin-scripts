#!/usr/bin/env bash
##
## Failed Backup v1.0
## The script functions to notify Windows admins and Storage admins on failed backups
## and also provide advisory to the respective infrastructure admins to know what to manage
##
## Date:	Jan-24-2018
## Author:	Tony Jonah
##

function sendMail()
{
	client=$1
	grp=$2
	log=$3
	address=$4
	echo "From: lagadmin@chevrontexaco.com" >> /tmp/mail_temp
	echo "To: $address" >> /tmp/mail_temp
	echo "Cc: lagadmin@chevrontexaco.com " >> /tmp/mail_temp
	echo "Subject: Failed Backup reports for $client in $grp " >> /tmp/mail_temp
	echo "MIME-Version: 1.0" >> /tmp/mail_temp
	echo "Content-Type: text/html" >> /tmp/mail_temp
	echo "<html>" >> /tmp/mail_temp
	echo "<head>" >> /tmp/mail_temp
	echo "<title> Failed Backup reports for $client in $grp </title>" >> /tmp/mail_temp
	echo "</head>" >> /tmp/mail_temp
	echo "<body>" >> /tmp/mail_temp
	echo "<h2>Please Check the Backup Issue on $client in $grp</h2>" >> /tmp/mail_temp
	cat $log >> /tmp/mail_temp
	echo "</body>" >> /tmp/mail_temp
	echo "</html>" >> /tmp/mail_temp

	cat /tmp/mail_temp | /usr/sbin/sendmail -t 	
	rm -rf /tmp/mail_temp
}


## Report failures
function reportFailures()
{
	client=$1
	grp=$2

	nsrsgrpcomp -c $client $grp > /tmp/${client}_${grp}  2>&1
	cat /tmp/${client}_${grp} | grep -i "failed\|Connection refused" > /tmp/${client}_${grp}_failed 2>&1
	if [ $? -eq 0 ];
	then
		/bin/grep "lagnrw8\|lagnrw12\|lawpjd\|lawnjd" /tmp/${client}_${grp}_failed 2>&1
		if [ $? -eq 0 ];
		then
			address=$windows
			grep "Connection refused" /tmp/${client}_${grp}_failed 2>&1
			if [ $? -eq 0 ];
			then 
				echo -e "\n<em style='color:red;'>Please can you confirm that the networker service is running<em>\n" >> /tmp/${client}_${grp}_failed 2>&1; 
			fi
		else
			address=$storage
		fi
		log="/tmp/${client}_${grp}_failed"
		sendMail $client $grp $log $address
	fi
	rm -rf /tmp/${client}_${grp} /tmp/${client}_${grp}_failed
}


##
main() 
{
	for line in $(echo print NSR client | nsradmin -s lklnx376 | grep "name:\|retention policy:\|group:\|save set:" |grep -v "NDMP array\|NAS device management\|storage replication policy\|Probe resource\|DISCONTINUED"| sed 's/ *retention policy: //g' | sed 's/ *group: //g' | sed 's/ *save set: //g' | tr  \\n \\t | sed 's/ *name: //' | sed 's/ *name: /\n/g' | sed 's/;//g'|grep -v lagnrnt3|sort|uniq);

	do 
		# Get the name of the client
		client=$(echo $line|awk '{print $1}');

		# Get the first group 
		groupa=$(echo $line|awk '{print $4}');
		if [[ $groupa =~ ^\/.*  ]] || [[ $groupa =~ ^\".:\\* ]] || [[ $groupa = "All" ]] || [[ $groupa = "Parallel" ]] || [[ $groupa = "save" ]] || [[ $groupa = "MSSQL" ]] || [[ $groupa = "Days" ]] || [[ $groupa = "recycle" ]];
		then
			continue
		fi

		# Get instances where the client has two groups
		groupb=$(echo $line|awk '{print $5}');
		if [[ $groupb =~ ^\/.*  ]] || [[ $groupb =~ ^\".:\\* ]] || [[ $groupb = "All" ]] || [[ $groupb = "Parallel" ]] || [[ $groupb = "save" ]] || [[ $groupb = "MSSQL" ]] || [[ $groupb = "Days" ]] || [[ $groupb = "recycle" ]];
		then
			for grp in $groupa;
			do
				reportFailures $client $grp
			done
			continue
		fi

		# Get instances where the client has three groups
		groupc=$(echo $line|awk '{print $6}');	
		if [[ $groupc =~ ^\/.*  ]] || [[ $groupc =~ ^\".:\\* ]] || [[ $groupc = "All" ]] || [[ $groupc = "Parallel" ]] || [[ $groupc = "save" ]] || [[ $groupc = "MSSQL" ]] || [[ $groupc = "Days" ]] || [[ $groupc = "recycle" ]];
		then
			for grp in $group{a..b};
			do
				reportFailures $client $grp
			done
			continue
		fi

		# Get instances where the client has four groups
		groupd=$(echo $line|awk '{print $7}');
		if [[ $groupd =~ ^\/.*  ]] || [[ $groupd =~ ^\".:\\* ]] || [[ $groupd = "All" ]] || [[ $groupd = "Parallel" ]] || [[ $groupd = "save" ]] || [[ $groupd = "MSSQL" ]] || [[ $groupd = "Days" ]] || [[ $groupd = "recycle" ]];
		then
			for grp in $group{a..c};
			do
				reportFailures $client $grp
			done
			continue
		fi

		# Get instances where the client has five groups
		groupe=$(echo $line|awk '{print $8}');
		if [[ $groupe =~ ^\/.*  ]] || [[ $groupe =~ ^\".:\\* ]] || [[ $groupe = "All" ]] || [[ $groupe = "Parallel" ]] || [[ $groupe = "save" ]] || [[ $groupe = "MSSQL" ]] || [[ $groupe = "Days" ]] || [[ $groupe = "recycle" ]];
		then
			for grp in $group{a..d};
			do
				reportFailures $client $grp
			done
			continue
		fi

		# Get instances where the client has six groups
		groupf=$(echo $line|awk '{print $8}');
		if [[ $groupf =~ ^\/.*  ]] || [[ $groupf =~ ^\".:\\* ]] || [[ $groupf = "All" ]] || [[ $groupf = "Parallel" ]] || [[ $groupf = "save" ]] || [[ $groupf = "MSSQL" ]] || [[ $groupf = "Days" ]] || [[ $groupf = "recycle" ]];
		then
			for grp in $group{a..e};
			do
				reportFailures $client $grp
			done
			continue
		fi

		# Get instances where the client has seven groups
		for grp in $group{a..f};
		do
			reportFailures $client $grp
		done
	done;
}


OIFS=$IFS;
IFS=$'\n';
windows="szwq iahg ijeo akoa olud lecq feeq"
storage="osez iabm"
main
IFS=$OIFS
