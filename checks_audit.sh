#!/usr/bin/env bash
for host in `cat /tmp/mymachines`;
do 
	echo "$host" >> /tmp/resultset;
	/sysadm/sbin/ssh2s $host "/util/iptroll/bin/iptroll &" 
	sleep 90;
	ssh -q $host grep "FAILED" /sysadm/iptroll/workdir/status.d >> /tmp/resultset;
	echo " " >> /tmp/resultset;
done;
