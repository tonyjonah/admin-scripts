#!/bin/sh

#
# satellite/proxy server
#
satserver="lklnx263.lagnr.chevrontexaco.net"
satmasterserver="rhsmaster.xhl.chevrontexaco.net"

#
# list of kickstart profiles
#
profiles=(
#'rhel6_ltd_houston_prod'
#'lagnr-ltd-rhel6'
#'rhel6_ltd_lagnr_prod_two'
'rhel6_ltd_lagnr_prod_new'
)

if [ `whoami` != "root" ]
then
	echo "You must be root in order to create kickstart ISO/IMG files."
	exit
fi

echo "Creating a new boot image for Red Hat Kickstarts."
echo ""
echo "Using $satserver as our Satellite Proxy Server"
echo ""
echo "The following profiles will be selectable when booting:"
echo "${profiles[@]}"

#
# final location of the boot ISO
#
isopath="/tmp/${satserver}.iso"

#
# final location of USB image
#
imgpath="/${satserver}.img"

#
# create a temporary directory
#
tmpdir=/tmp/iso.$$
tmpimgdir=/img.$$
mkdir -p $tmpdir $tmpimgdir

#
# create isolinux.cfg and boot.msg
#
cat <<_EOF > ${tmpdir}/isolinux.cfg
default local
prompt 1
timeout 0
display boot.msg

label local
localboot 1

_EOF

echo -e "\n\n\n\n\n\n\n\n                Chevron Kickstart ISO\n" > ${tmpdir}/boot.msg
echo -e "Kickstart profiles (via $satserver):\n" >> ${tmpdir}/boot.msg

#
# add profiles to isolinux.cfg and boot.msg
#
for (( i=0; i<${#profiles[@]}; i++)); do

   #
   # URL of kickstart config file
   #
   kscfg="http://${satserver}/ks/cfg/org/1/label/${profiles[${i}]}"
   #
   # find kickstart tree
   #
   kstree=$(wget -q $kscfg -O - | awk '/^url/ {print $3}')

   #
   # retrieve required files from kickstart tree
   #
   wget -q $kstree/isolinux/vmlinuz -O $tmpdir/$i.krn
   wget -q $kstree/isolinux/initrd.img -O $tmpdir/$i.img

   #
   # need one copy of isolinux.bin
   #
   [ -f "$tmpdir/isolinux.bin" ] ||
      wget -q $kstree/isolinux/isolinux.bin -O $tmpdir/isolinux.bin

   #
   # add profile to isolinux.cfg
   #
   echo "label ks${i}" >> ${tmpdir}/isolinux.cfg
   echo "kernel ${i}.krn" >> ${tmpdir}/isolinux.cfg
   echo -e "append initrd=${i}.img text noipv6 noselinux rdblacklist=nouveau ks=${kscfg}\n" >> ${tmpdir}/isolinux.cfg

   #
   # add profile to boot.msg
   #
   echo -e "   ks${i}   ${profiles[${i}]}" >> ${tmpdir}/boot.msg

done 

#
# finalize boot.msg
#
cat <<_EOF >> ${tmpdir}/boot.msg

To start kickstart process, enter the following at the prompt:

   <kickstart-profile> [ksdevice=<nic>] [hostname=<hostname>] [ip=<ip>]
   [netmask=<netmask>] [gateway=<gateway>] [dns=<nameserver1,nameserver2>]

Example for static IP machines:
   ks0 ksdevice=eth0 hostname=myhost.example.com ip=192.168.1.100
   netmask=255.255.255.0 gateway=192.168.1.1 dns=192.168.1.1,192.168.1.2

Example for machines with DHCP:
   ks0
or
   ks0 hostname=myhost.example.com

_EOF

#
# create custom ISO
#
mkisofs -U -J -l -R -r -T -V 'Custom Boot ISO' \
   -b isolinux.bin -c boot.cat \
   -no-emul-boot -boot-load-size 4 -boot-info-table \
   -o $isopath $tmpdir &>/dev/null

#
# Create USB images
#

wget -q $satmasterserver/pub/linuxBootKey.img -O $imgpath
loopdevice=`losetup -f`
losetup $loopdevice $imgpath &>/dev/null
loopmount="/dev/mapper/`kpartx -av $loopdevice | cut -f 3 -d ' '`"
sleep 3

mount -t vfat $loopmount $tmpimgdir
cp -r ${tmpdir}/* ${tmpimgdir}/
mv ${tmpimgdir}/isolinux.cfg ${tmpimgdir}/syslinux.cfg

umount $tmpimgdir &>/dev/null
kpartx -dv $loopdevice &>/dev/null
losetup -d $loopdevice &>/dev/null
sync &>/dev/null

echo "Done"
echo ""
echo "iso image is available at $isopath"
echo "img image is available at $imgpath"

echo ""
echo "To create a bootable USB key with the img file, execute the following command as root (assuming sdc is your USB key device):"
echo "	dd if=$imgpath of=/dev/sdc"

#
# done
#


