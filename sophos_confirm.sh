#!/usr/bin/env bash
##
##
##

d_loc=/users/tqfl/sophos_data_areas
log=/users/tqfl/scan_log
OFS=$IFS;
rm -f $log
IFS=$'\n';
found=0;
for area in `cat /domain/src/amd.meta.data|awk '{print $1}'|grep -v "##\|#"`;
do
	c_area=/data/${area};
	for s_area in `cat ${d_loc}`;
	do
		if [ "${c_area}" = "${s_area}" ];
		then
			echo "${s_area} is being scanned" >> ${log} 2>&1;
			found=1;
			break;
		else
			found=0;
			continue;
		fi
	done;
	if [ $found -eq 0 ];
	then
		#echo "${c_area} is not being scanned" >> ${log} 2>&1;
		echo "${c_area}" >> ${log} 2>&1;
	fi
done;
echo -e "check has been completed";
IFS=$OFS
