#!/usr/bin/env bash
##
## Author : Tony Jonah
## Date   : 26th of August, 2015
## This script runs all the components  required to fix iptroll modules
##
/data/sysadm/scripts/fix_hosts.sh 2> /data/sysadm/iptroll/logs/iptroll_fix_log && /data/sysadm/scripts/gather_mods.sh 2> /data/sysadm/iptroll/logs/iptroll_fix_log && /data/sysadm/scripts/iptroll_silent.sh 2> /data/sysadm/iptroll/logs/iptroll_fix_log
