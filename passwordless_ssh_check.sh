#!/bin/bash
##
## 
##
list="eclnx01 eclnx02 gblnx01 lklnx214 lklnx215 lklnx224 lklnx227 lklnx228 lklnx250 lklnx258 lklnx259 lklnx260 lklnx261 lklnx262 lklnx308 lklnx358 lklnx364 lklnx378 lklnx379 lklnx380 lklnx536"
function login()
{
	echo "##################";	
	echo "Running Test on $1";
	echo "##################";	
	for host in $list;
	do
		if [[ "$host" = "$1" ]];
		then
			continue;
		fi
		
		ssh -q $1 echo "Greetings $(ssh -q $host hostname 2>&1)";
		ssh -q $1 echo $(ssh -q $host uname -a 2>&1);
		echo "";
	done;
	return;
}

main () 
{
	for server in $list;
	do
		login $server;
	done;
}

main;
