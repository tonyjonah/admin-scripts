#!/bin/bash
##########################################################################
##                                                                      ##
##                                                                      ##
## Author: Tony Jonah                                                   ##
## Date  : 30/12/2013                                                   ##
##                                                                      ##
## The script carries out specified module checks. The list of hosts    ##
## the script checks against should be placed withing the same folder   ##
## The script should be run as root. Its logs are kept in the same      ##
## directory and can be identified with the date and time               ##
##                                                                      ##
##########################################################################
#
# Variables definition area
#
TIME="`date '+%d%b%y'`"
var1="files_sgid2"
var2="files_suid2"
var3="files_ww"
var4="root_sgid2"
var5="root_suid2"
var6="os_yppasswd_filter"
var7="os_banner_issue"
var8="os_banner_issue_ssh"
var9="os_banner_gui"
var10="os_perms_sys"
var11="os_pwd_rl_one"
var12="os_fingerprint2"
var13="os_banner_sendmail"
DIR="/util/iptroll/modules.d/"
LOG="iptroll".$TIME
RET=0
##
## Section where the functions housing the Iptroll checks and fixes are actually implemented
##
function1()
{

        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var1"
        read HOSTSLIST
	for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var1"  >> $LOG.$var1 2>&1
                /usr/bin/ssh -q $host $DIR/$var1 -c  >> $LOG.$var1 2>&1
                /usr/bin/ssh -q $host $DIR/$var1 -f  >> $LOG.$var1 2>&1
        done;
}
function2()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var2"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var2"  >> $LOG.$var2 2>&1
                /usr/bin/ssh -q $host $DIR/$var2 -c  >> $LOG.$var2 2>&1
               	/usr/bin/ssh -q $host $DIR/$var2 -f  >> $LOG.$var2 2>&1
        done;
}
function3()
{
	printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var3"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var3"  >> $LOG.$var3 2>&1
                /usr/bin/ssh -q $host $DIR/$var3 -c  >> $LOG.$var3 2>&1
                /usr/bin/ssh -q $host $DIR/$var3 -f  >> $LOG.$var3 2>&1
        done;
}
function4()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var4"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var4"  >> $LOG.$var4 2>&1
                /usr/bin/ssh -q $host $DIR/$var4 -c  >> $LOG.$var4 2>&1
                /usr/bin/ssh -q $host $DIR/$var4 -f  >> $LOG.$var4 2>&1
        done;
}
function5()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var5"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var5"  >> $LOG.$var5 2>&1
                /usr/bin/ssh -q $host $DIR/$var5 -c  >> $LOG.$var5 2>&1
                /usr/bin/ssh -q $host $DIR/$var5 -f  >> $LOG.$var5 2>&1
        done;
}
function6()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var6"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var6"  >> $LOG.$var6 2>&1
                /usr/bin/ssh -q $host $DIR/$var6 -c  >> $LOG.$var6 2>&1
                /usr/bin/ssh -q $host $DIR/$var6 -f  >> $LOG.$var6 2>&1
        done;
}
function7()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var7"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var7"  >> $LOG.$var7 2>&1
                /usr/bin/ssh -q $host $DIR/$var7 -c  >> $LOG.$var7 2>&1
                /usr/bin/ssh -q $host $DIR/$var7 -f  >> $LOG.$var7 2>&1
        done;
}
function8()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var8"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var8"  >> $LOG.$var8 2>&1
                /usr/bin/ssh -q $host $DIR/$var8 -c  >> $LOG.$var8 2>&1
                /usr/bin/ssh -q $host $DIR/$var8 -f  >> $LOG.$var8 2>&1
        done;
}
function9()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var9"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
		printf "\n**Working on host %s for module %s \n" "$host" "$var9"  >> $LOG.$var9 2>&1
                /usr/bin/ssh -q $host $DIR/$var9 -c  >> $LOG.$var9 2>&1
                /usr/bin/ssh -q $host $DIR/$var9 -f  >> $LOG.$var9 2>&1
        done;
}
function_10()
{
	printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var10"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
		printf "\n**Working on host %s for module %s \n" "$host" "$var10"  >> $LOG.$var10 2>&1
                /usr/bin/ssh -q $host $DIR/$var10 -c  >> $LOG.$var10 2>&1
                /usr/bin/ssh -q $host $DIR/$var10 -f  >> $LOG.$var10 2>&1
	done;
}
function_11()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var11"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var11"  >> $LOG.$var11 2>&1
                /usr/bin/ssh -q $host $DIR/$var11 -c  >> $LOG.$var11 2>&1
                /usr/bin/ssh -q $host $DIR/$var11 -f  >> $LOG.$var11 2>&1
        done;
}
function_12()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var12"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var12"  >> $LOG.$var12 2>&1
                /usr/bin/ssh -q $host $DIR/$var12 -c  >> $LOG.$var12 2>&1
                /usr/bin/ssh -q $host $DIR/$var12 -f  >> $LOG.$var12 2>&1
        done;
}
function_13()
{
        printf "\n** Enter the file with the list of the hosts to be remediated for module %s : " "$var13"
        read HOSTSLIST
        for host in `cat ./$HOSTSLIST`
        do
                printf "\n**Working on host %s for module %s \n" "$host" "$var13"  >> $LOG.$var13 2>&1
                /usr/bin/ssh -q $host $DIR/$var13 -c  >> $LOG.$var13 2>&1
                /usr/bin/ssh -q $host $DIR/$var13 -f  >> $LOG.$var13 2>&1
        done;
}
#
# Script begins here
#
printf "** This script runs a specified module on all hosts listed \n"
printf "** in the files. Please confirm that the workstation or \n"
printf "** server being checked are functional and on the network \n"
printf "** \n"
printf "** \n"
sleep 3s
while [ $RET -eq 0 ]
do
printf "** \n"
printf "** \n"
printf "** \n"
printf "** Choose the module which you want to run the the script for \n"
printf "** For the %s module choose option             	1 \n" "$var1"
printf "** For the %s module choose option             	2 \n" "$var2"
printf "** For the %s module choose option             		3 \n" "$var3"
printf "** For the %s module choose option             		4 \n" "$var4"
printf "** For the %s module choose option             		5 \n" "$var5"
printf "** For the %s module choose option     		6 \n" "$var6"
printf "** For the %s module choose option     		7 \n" "$var7"
printf "** For the %s module choose option     	8 \n" "$var8"
printf "** For the %s module choose option     		9 \n" "$var9"
printf "** For the %s module choose option     		10 \n" "$var10"
printf "** For the %s module choose option             	11 \n" "$var11"
printf "** For the %s module choose option             	12 \n" "$var12"
printf "** For the %s module choose option             	13 \n" "$var13"
printf "** To quit the script choose option				q or Q \n"
read modules
sleep 3s
case $modules in
1)
	printf "**\n Iptroll will run for module " "%s \n" "$var1"
	function1
	;;
2)
        printf "**\n Iptroll will run for module " " %s \n" "$var2"
        function2 
	;;
3)
	printf "**\n Iptroll will run for module " " %s \n" "$var3"
        function3 
	;;
4)
	printf "**\n Iptroll will run for module " " %s \n" "$var4"
        function4 
	;;
5)
        printf "**\n Iptroll will run for module " " %s \n" "$var5"
        function5 
	;;
6)
        printf "**\n Iptroll will run for module " " %s \n" "$var6"
        function6 
	;;
7)
        printf "**\n Iptroll will run for module " " %s \n" "$var7"
        function7 
	;;
8)
	printf "**\n Iptroll will run for module " " %s \n" "$var8"
        function8 
	;;
9)
	printf "**\n Iptroll will run for module " " %s \n" "$var9"
        function9 
	;;
10)
	printf "**\n Iptroll will run for module " " %s \n" "$var10"
        function_10 
	;;
11)
        printf "**\n Iptroll will run for module " " %s \n" "$var11"
        function_11 
	;;
12)
        printf "**\n Iptroll will run for module " " %s \n" "$var12"
        function_12 
	;;
13)
        printf "**\n Iptroll will run for module " " %s \n" "$var13"
        function_13 
	;;
q|Q)
	printf "**\n Good bye :) "
	exit 0
	;;
*)
	printf "**\n An invalid option was entered"
	printf "**\n The script will exit now "
	exit 0
	;;
esac
done;
