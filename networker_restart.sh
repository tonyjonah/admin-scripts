#!/bin/bash

##########################################################
##							##
## Script for restarting networker on lkus46		##
## 							##
## Author: Tony Jonah					##
## Created: 10th of January, 2014			##
##							##
##########################################################

## Variables Declaration
date="`date '+%d%b%y'`"

printf "\nHave all backup, cloning and staging jobs been stopped : [y/n]"
read choice
if [ $choice == n ]; then
	printf "\nNow killing cloning"
	ps -ef|grep bacln > /users/tqfl/backup_reports/bacln.$date
	/sysadm/scripts/killbaclnpid
	sleep 4
	printf "\nNow killing staging"
	ps -ef|grep stage > /users/tqfl/backup_reports/stage.$date
	/sysadm/scripts/killstagepid
	sleep 4
	printf "\nNow killing backups"
	ps -ef|grep savegrp > /users/tqfl/backup_reports/save.$date
        /sysadm/scripts/killsavepid
	sleep 4
elif [ $choice == y ]; then
	printf "\nNetworker will be stopped now"
else 
	printf "\nWrong entry, halting script now"
	exit 1
fi
## Networker service termination
/etc/init.d/networker stop
printf "\nNetworker service stopped"
sleep 1m
## Create new nsr/tmp file
cd /nsr/;mv tmp tmp.$date
printf "\n/nsr/tmp has been moved"
sleep 1m
## Create new /nsr/res/jobsdb
cd /nsr/res;mv jobsdb jobsdb.$date
printf "\n/nsr/res/jobsdb has been moved"
sleep 1m

## Restarting networker now
/etc/init.d/networker start

printf "\n\nDone!!!!, Good bye :)"
